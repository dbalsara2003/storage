import datetime
import json
import threading
from threading import Thread
import pykafka
from pykafka import KafkaClient
from pykafka.common import OffsetType

import connexion
from connexion import NoContent
from requests import session
import swagger_ui_bundle

import mysql.connector 
import pymysql
import yaml
import logging
import logging.config

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, session
from base import Base
from buy import Buy
from sell import Sell

with open('./app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

DB_ENGINE = create_engine(f"mysql+pymysql://{app_config['user']}:{app_config['password']}@{app_config['hostname']}:{app_config['port']}/{app_config['db']}", pool_pre_ping=True)
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

def process_messages():
    client = KafkaClient(hosts=f"{app_config['events']['hostname']}:{app_config['events']['port']}")
    topic = client.topics[app_config['events']['topic']]

    messages = topic.get_simple_consumer( 
        reset_offset_on_start = False, 
        auto_offset_reset = OffsetType.LATEST
    )

    for msg in messages:
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        payload = msg['payload']
        msg_type = msg['type']

        session = DB_SESSION()

        logger.info("CONSUMER::storing buy event")
        logger.info(msg)

        if msg_type == 'buy':
            buy = Buy(
                payload['buy_id'],
                payload['item_name'],
                payload['item_price'],
                payload['buy_qty'],
                payload['trace_id'],
            )
            session.add(buy)
        elif msg_type == 'sell':
            sell = Sell(
                payload['buy_id'],
                payload['item_name'],
                payload['item_price'],
                payload['buy_qty'],
                payload['trace_id'],
            )
            session.add(sell)

        session.commit()
        session.close()

    messages.commit_offsets()

def buy(body):
    new_buy = Buy(
        body['buy_id'],
        body['item_name'],
        body['item_price'],
        body['buy_qty'],
        body['trace_id']
    )
    session = DB_SESSION()

    session.add(new_buy)
    session.commit()
    session.close() 
    
    logger.debug(f"Stored buy event with trace id {body['trace_id']}")

    return NoContent, 201

def get_buys(timestamp):
    session = DB_SESSION()

    rows = session.query(Buy).filter(Buy.date_created >= timestamp)

    data = []

    data = [row.to_dict() for row in rows]
    session.commit()
    session.close()
    logger.debug(f"Request to get_buys with timestamp {timestamp} returned {len(data)} results")

    return data, 200

def sell(body):
    new_sell = Sell(
        body['sell_id'],
        body['item_name'],
        body['item_price'],
        body['sell_qty'],
        body['trace_id']
    )

    session = DB_SESSION()

    session.add(new_sell)
    session.commit()
    session.close() 

    logger.debug(f"Stored sell event with trace id {body['trace_id']}")

 
    return NoContent, 201

def get_sells(timestamp):
    session = DB_SESSION

    rows = session.query(Sell).filter(Sell.date_created >= timestamp)

    data = []

    data = [row.to_dict() for row in rows]
    session.commit()
    session.close()
    logger.debug(f"Request to get_sells with timestamp {timestamp} returned {len(data)} results")


    return data, 200

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api('openapi.yaml', strict_validation=True, validate_responses=True)

logger = logging.getLogger('basic')

if __name__ == "__main__":
    tl = Thread(target=process_messages)
    tl.daemon = True
    tl.start()
    app.run(port=8090)
